/*
* Copyright (c) Lutron. All rights reserved.
*/
package com.example.githubexplorer.room.database

/**
 * Created by hmjain on 29/08/21.
 */
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.githubexplorer.room.dao.RepoDao
import com.example.githubexplorer.room.entity.RepoEntity

private const val DB_NAME = "githubExplorer"

@Database(entities = [RepoEntity::class], version = 1)
abstract class RepoDatabase : RoomDatabase() {
    abstract val repoDao: RepoDao?

    companion object {
        private var repoDB: RepoDatabase? = null
        fun getInstance(context: Context): RepoDatabase? {
            if (null == repoDB) {
                repoDB = buildDatabaseInstance(context)
            }
            return repoDB
        }

        private fun buildDatabaseInstance(context: Context): RepoDatabase {
            return Room.databaseBuilder(
                context,
                RepoDatabase::class.java,
                DB_NAME
            ).allowMainThreadQueries().build()
        }
    }
}