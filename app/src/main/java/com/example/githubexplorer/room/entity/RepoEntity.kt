/*
* Copyright (c) Lutron. All rights reserved.
*/
package com.example.githubexplorer.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by hmjain on 29/08/21.
 */
@Entity(tableName = "Repo")
data class RepoEntity(@PrimaryKey val id:Long, val url:String)