package com.example.githubexplorer.room.interfaces

import com.example.githubexplorer.model.Repo
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubExplorerDataService {

    @GET("/users/{Username}/repos")
    suspend fun getRepos(@Path("Username") userName: String): List<Repo>
}