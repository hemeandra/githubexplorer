/*
* Copyright (c) Lutron. All rights reserved.
*/
package com.example.githubexplorer.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.githubexplorer.room.entity.RepoEntity

/**
 * Created by hmjain on 29/08/21.
 */
@Dao
interface RepoDao {

    @Insert
    fun insertAll(repos: List<RepoEntity>)

    @Insert
    fun insert(repo: RepoEntity)

    @Query("delete from Repo")
    fun deleteAllRepos()
}