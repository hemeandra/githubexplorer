package com.example.githubexplorer.repo

import android.content.Context
import com.example.githubexplorer.room.database.RepoDatabase
import com.example.githubexplorer.room.interfaces.GithubExplorerDataService
import com.example.githubexplorer.model.Repo
import com.example.githubexplorer.room.entity.RepoEntity
import com.example.githubexplorer.network.RetrofitInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.UUID

class MainRepo {

    suspend fun getRepos(userName: String, context: Context): List<Repo>? = withContext(Dispatchers.IO) {
        return@withContext try {
            val repos = RetrofitInstance.retrofitInstance.create(GithubExplorerDataService::class.java).getRepos(
                userName
            )
            repos.let{
                val repoEntities: List<RepoEntity> = repos.map {
                    RepoEntity(it.id, it.url)
                }
                RepoDatabase.getInstance(context)?.repoDao?.insertAll(repoEntities)
            }
            repos
        } catch (ex: Exception) {
            null
        }
    }

    suspend fun addRepoToDB(url:String, context: Context) : Repo = withContext(Dispatchers.IO) {
        val id = UUID.randomUUID().mostSignificantBits and Long.MAX_VALUE
        val repoEntity = RepoEntity(id, url)
        RepoDatabase.getInstance(context)?.repoDao?.insert(repoEntity)
        return@withContext Repo(id, url)
    }

    suspend fun deleteFromDb(context: Context) = withContext(Dispatchers.IO) {
        RepoDatabase.getInstance(context)?.repoDao?.deleteAllRepos()
    }
}