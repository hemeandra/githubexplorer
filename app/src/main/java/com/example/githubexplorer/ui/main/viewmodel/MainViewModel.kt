package com.example.githubexplorer.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.githubexplorer.model.Repo
import com.example.githubexplorer.repo.MainRepo
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private var _repoLiveData: MutableLiveData<List<Repo>?> = MutableLiveData<List<Repo>?>()
    private val repos = mutableListOf<Repo>()

    val repoLiveData: LiveData<List<Repo>?>
        get() = _repoLiveData

    fun getRepos(userName: String) {
        viewModelScope.launch {
            val repoList = MainRepo().getRepos(userName, getApplication())
            repoList?.let {
                repos.clear()
                MainRepo().deleteFromDb(getApplication())
                repos.addAll(it)
                _repoLiveData.value = it
            }
        }
    }

    fun getFilteredRepo(searchWord: String?): List<Repo> {
        return if (searchWord.isNullOrEmpty()) {
            repos
        } else {
            repos.filter {
                it.url.contains(searchWord, true)
            }
        }
    }

    fun addRepoToDB(repoUrl: String) {
        viewModelScope.launch {
            repos.add(MainRepo().addRepoToDB(repoUrl, getApplication()))
            _repoLiveData.value = repos
        }
    }
}