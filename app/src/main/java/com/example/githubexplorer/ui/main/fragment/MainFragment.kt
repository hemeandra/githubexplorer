package com.example.githubexplorer.ui.main.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.githubexplorer.R
import com.example.githubexplorer.adapter.RepoAdapter
import com.example.githubexplorer.ui.main.viewmodel.MainViewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recylerView)
        val searchView = view.findViewById<SearchView>(R.id.searchView)
        val edtTxt = view.findViewById<EditText>(R.id.edtUserName)
        val edtNewRepoToAdd = view.findViewById<EditText>(R.id.edtNewRepoToAdd)
        val lytAddRepo = view.findViewById<RelativeLayout>(R.id.lytAddRepo)
        val imgAdd = view.findViewById<ImageView>(R.id.imgAddRepo)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        val adapter = RepoAdapter(mutableListOf())
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        viewModel.repoLiveData.observe(
            viewLifecycleOwner,
            {
                it?.let { repos ->
                    searchView.visibility = View.VISIBLE
                    lytAddRepo.visibility = View.VISIBLE
                    adapter.setData(repos)
                }
            }
        )
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.setData(viewModel.getFilteredRepo(newText))
                return true
            }
        })
        edtTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                viewModel.getRepos(s.toString())
            }

        })

        imgAdd.setOnClickListener {
            val url = edtNewRepoToAdd.text.toString()
            if (url.isNotEmpty()) {
                viewModel.addRepoToDB(edtNewRepoToAdd.text.toString())
                edtNewRepoToAdd.setText("")
                Toast.makeText(requireContext(), "added to DB", Toast.LENGTH_SHORT).show()
            }
        }
        return view
    }
}