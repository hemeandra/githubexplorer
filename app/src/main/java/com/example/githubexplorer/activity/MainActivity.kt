package com.example.githubexplorer.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.githubexplorer.R
import com.example.githubexplorer.ui.main.fragment.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, MainFragment.newInstance())
            .commitNow()
    }
}