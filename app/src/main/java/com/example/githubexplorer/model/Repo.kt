package com.example.githubexplorer.model

data class Repo(val id:Long, val url:String)
